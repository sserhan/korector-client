package org.parisnanterre.korector.client.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Client;
import org.parisnanterre.korector.client.entities.IntegrationNote;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.parisnanterre.korector.client.entities.ProjetIntegration;
import org.parisnanterre.korector.client.modules.users.feigns.UsersClient;
import org.parisnanterre.korector.client.modules.users.payloads.requests.UserResponse;
import org.parisnanterre.korector.client.modules.users.payloads.responses.UserForm;
import org.parisnanterre.korector.client.modules.users.payloads.responses.UserImportResponse;
import org.parisnanterre.korector.client.payload.request.AnalyseForm;
import org.parisnanterre.korector.client.payload.response.AnalyseResponse;
import org.parisnanterre.korector.client.config.KeycloakConfig;
import org.parisnanterre.korector.client.feigns.IntegrationClient;
import org.parisnanterre.korector.client.payload.response.ClientUserResponse;
import org.parisnanterre.korector.client.providers.github.payloads.GithubRepoResponse;
import org.parisnanterre.korector.client.repositories.IntegrationNoteRepository;
import org.parisnanterre.korector.client.repositories.ProjetIntegrationRepository;
import org.parisnanterre.korector.client.service.ValidationExceptionsService;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/modules")
public class UserControllerV1 {

    private final KeycloakConfig config;
    private final ProjetIntegrationRepository projetIntegrationRepository;
    private final IntegrationClient integrationClient;
    private final UsersClient usersClient;
    private final IntegrationNoteRepository integrationNoteRepository;
    private final ValidationExceptionsService validationExceptionsService;


    public UserControllerV1(KeycloakConfig config, ProjetIntegrationRepository projetIntegrationRepository, IntegrationClient integrationClient, UsersClient usersClient, IntegrationNoteRepository integrationNoteRepository, ValidationExceptionsService validationExceptionsService) {
        this.config = config;
        this.projetIntegrationRepository = projetIntegrationRepository;
        this.integrationClient = integrationClient;
        this.usersClient = usersClient;
        this.integrationNoteRepository = integrationNoteRepository;
        this.validationExceptionsService = validationExceptionsService;
    }


    @GetMapping("/users")
    public ResponseEntity<List<UserResponse>> all(@AuthenticationPrincipal Jwt jwt){
        return new ResponseEntity<>(usersClient.all(jwt.getTokenValue()), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserResponse> getOne(@AuthenticationPrincipal Jwt jwt, @PathVariable String id){
        return new ResponseEntity<>(usersClient.getOne(id,jwt.getTokenValue()), HttpStatus.OK);
    }

    @GetMapping("/users/managers")
    private ResponseEntity<List<UserResponse>> managers(@AuthenticationPrincipal Jwt jwt){
        List<UserResponse> users = this.all(jwt).getBody();
        assert users != null;
        List<UserResponse> managers = users
                .stream()
                .filter(user -> user.getRoles().contains("MANAGER"))
                .collect(Collectors.toList());
        return new ResponseEntity<>(managers, HttpStatus.OK);

    }

    @PostMapping("/users")
    public ResponseEntity<UserResponse> create(@AuthenticationPrincipal Jwt jwt, @Valid @RequestBody UserForm userForm){
        return new ResponseEntity<>(usersClient.save(userForm, jwt.getTokenValue()), HttpStatus.CREATED);
    }

    @PutMapping("/users")
    public ResponseEntity<UserResponse> update(@AuthenticationPrincipal Jwt jwt, @Valid @RequestBody UserForm userForm){
        return new ResponseEntity<>(usersClient.update(userForm, jwt.getTokenValue()), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> delete(@AuthenticationPrincipal Jwt jwt, @PathVariable String id){
        usersClient.delete(id,jwt.getTokenValue());
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("/users/import")
    public ResponseEntity<List<UserImportResponse>> importCsvFile(@RequestParam("file") MultipartFile file){
        return new ResponseEntity<>(usersClient.uploadFile(file), HttpStatus.ACCEPTED);
    }

    @GetMapping("/users/repos")
    public List<GithubRepoResponse> repos(@AuthenticationPrincipal Jwt jwt) throws IOException {
        List<GithubRepoResponse> repos = new ArrayList<>();
        GitHub github = new GitHubBuilder().withOAuthToken(getGithubToken(jwt.getTokenValue())).build();
        github.getMyself().getRepositories().forEach((k,v) -> {
            GithubRepoResponse githubRepoResponse = new GithubRepoResponse();
            githubRepoResponse.setName(v.getFullName());
            githubRepoResponse.setProvider("github");
            repos.add(githubRepoResponse);
        });
        return repos;
    }

    @GetMapping("/users/roles")
    public ResponseEntity<List<String>> getRoles(@AuthenticationPrincipal Jwt jwt){
        return new ResponseEntity<>(usersClient.getRoles(jwt.getTokenValue()), HttpStatus.OK);
    }

    @GetMapping("users/repos/branches")
    public List<String> branches(@AuthenticationPrincipal Jwt jwt, @RequestParam("name") String name) throws IOException {
        List<String> branches = new ArrayList<>();
        GitHub github = new GitHubBuilder().withOAuthToken(getGithubToken(jwt.getTokenValue())).build();
        github.getRepository(name).getBranches().forEach((k,v) -> branches.add(v.getName()));
        return branches;

    }

    @GetMapping("/users/userRolesInfo")
    public ResponseEntity<ClientUserResponse> getUserRolesInfo() {
        ClientUserResponse userRolesInfo = new ClientUserResponse();
        Set<String> roles = new HashSet<>();
        SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities()
                .forEach(x -> roles.add(x.toString()));
        userRolesInfo.setRoles(roles);

        return new ResponseEntity<>(userRolesInfo,HttpStatus.OK);
    }



    @GetMapping("/users/userInfoClient")
    public ResponseEntity<ClientUserResponse> getUserInfo(@AuthenticationPrincipal Jwt jwt) throws JsonProcessingException {
        String token = getGithubToken(jwt.getTokenValue());
        ClientUserResponse githubInfo = new ClientUserResponse();

        if (token != null && !token.contains("invalid")) {
            githubInfo = getGithubUser(getGithubToken(jwt.getTokenValue()));
            githubInfo.setGithub(true);
        } else {
            githubInfo.setGithub(false);
        }

        return new ResponseEntity<>(githubInfo,HttpStatus.OK);
    }

    @GetMapping("/integration/analyses/rebuild/{id}")
    public ResponseEntity<AnalyseResponse> rebuild(@AuthenticationPrincipal Jwt jwt, @PathVariable Long id){
        return new ResponseEntity<>(integrationClient.rebuild(getGithubToken(jwt.getTokenValue()), id), HttpStatus.OK);
    }

    private String getGithubToken(String jwt) {
        String url = config.getHost() + "/auth/realms/" + config.getRealm() + "/broker/github/token";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer " + jwt);
        try {
            HttpEntity request = new HttpEntity(headers);
            ResponseEntity<String> response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    request,
                    String.class,
                    1
            );
            if (response.getStatusCode() == HttpStatus.OK) {
                return response.getBody().substring(13, 53);
            }else return null;
        }catch (HttpClientErrorException e){
            return null;
        }
    }

    private ClientUserResponse getGithubUser(String token) throws JsonProcessingException {
        String url = "https://api.github.com/user";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer " + token);
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<String> response = new RestTemplate().exchange(
                url,
                HttpMethod.GET,
                request,
                String.class,
                1
        );
        if (response.getStatusCode() == HttpStatus.OK) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            JsonNode username = root.path("login");
            JsonNode avatarUrl = root.path("avatar_url");
            ClientUserResponse clientUserResponse = new ClientUserResponse();
            clientUserResponse.setAvatarUrl(avatarUrl.asText());
            clientUserResponse.setUsername(username.asText());
            return clientUserResponse;
        } else {
            return null;
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_view_analyze')")
    @PostMapping("/analyses")
    public AnalyseResponse buildAndSave(@AuthenticationPrincipal Jwt jwt, @RequestBody AnalyseForm analyseForm){
        AnalyseResponse analyseResponse = new AnalyseResponse();
        ProjetIntegration projetIntegration = projetIntegrationRepository.getByIdProjet(analyseForm.getProjetId());
        analyseResponse = integrationClient.buildAndSave(getGithubToken(jwt.getTokenValue()),analyseForm);
        Set<Long> analyses = projetIntegration.getAnalyses();
        if (analyses != null) {
            analyses.add(analyseResponse.getId());
            projetIntegration.setAnalyses(analyses);
        }else{
            analyses = new HashSet<>();
            analyses.add(analyseResponse.getId());
        }
        projetIntegrationRepository.save(projetIntegration);
        integrationNoteRepository.save(new IntegrationNote((analyseResponse.getId())));
        return analyseResponse;
    }

    //Formatage des erreurs de validation
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ArrayList<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return validationExceptionsService.getErrors(ex);
    }
}
