package org.parisnanterre.korector.client.api;

import org.parisnanterre.korector.client.entities.IntegrationNote;
import org.parisnanterre.korector.client.feigns.IntegrationClient;
import org.parisnanterre.korector.client.feigns.NotationClient;
import org.parisnanterre.korector.client.payload.request.NoteForm;
import org.parisnanterre.korector.client.payload.response.NoteResponse;
import org.parisnanterre.korector.client.repositories.IntegrationNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
public class NoteController {
    @Autowired
    private NotationClient notationClient;
    @Autowired
    private IntegrationClient integrationClient;
    @Autowired
    private IntegrationNoteRepository integrationNoteRepository;

    @GetMapping("/notes")
    public List<NoteResponse> getAllNotes(){
        return  notationClient.getAllNotes();
    }

    @GetMapping("/notes/analyse/{analyseId}")
    public List<NoteResponse> getNotesByAnalyse(@PathVariable Long analyseId){
        List<NoteResponse> noteResponses = new ArrayList<>();
        integrationNoteRepository.getByIdAnalyse(analyseId).getNotes()
                .forEach(note -> noteResponses.add(notationClient.getOneNotes(note)));
        return noteResponses;
    }

    @GetMapping("/notes/{id}")
    public NoteResponse getOneNotes(@PathVariable Long id){
        return notationClient.getOneNotes(id);
    }

    @PostMapping("/notes/noteTotal")
    public NoteResponse genererNote(@RequestBody NoteForm noteForm){
        return notationClient.genererNote(noteForm);
    }

    @PostMapping("/notes")
    public NoteResponse saveNote(@Valid @RequestBody NoteForm noteForm){
        IntegrationNote integrationNote = integrationNoteRepository.getByIdAnalyse(noteForm.getAnalyseId());
        NoteResponse noteResponse = notationClient.saveNote(noteForm);
        Set<Long> notes = integrationNote.getNotes();
        if(notes != null){
            notes.add(noteResponse.getId());
            integrationNote.setNotes(notes);
        }else{
            notes = new HashSet<>();
            notes.add(noteResponse.getId());
        }
        integrationNoteRepository.save(integrationNote);
        return noteResponse;
    }

    @PutMapping("/notes")
    public NoteResponse updateNote(@RequestBody NoteForm noteForm){
        return notationClient.updateNote(noteForm);
    }

    @DeleteMapping("/notes")
    public void deleteNote(@RequestParam Long id){
        List<IntegrationNote> integrationNote = integrationNoteRepository.findAll().stream()
                .filter(i -> i.getNotes() != null && !i.getNotes().isEmpty())
                .filter(i -> i.getNotes().contains(id))
                .collect(Collectors.toList());
        integrationNote.forEach(i -> {
            i.getNotes().remove(id);
            integrationNoteRepository.save(i);
        });
        notationClient.deleteNote(id);
    }
}
