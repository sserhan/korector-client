package org.parisnanterre.korector.client.api;

import org.parisnanterre.korector.client.feigns.IntegrationClient;
import org.parisnanterre.korector.client.feigns.NotationClient;
import org.parisnanterre.korector.client.payload.request.MetricForm;
import org.parisnanterre.korector.client.payload.response.MetricResponse;
import org.parisnanterre.korector.client.payload.response.ResultatScannerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
public class MetricController {
    @Autowired
    private NotationClient notationClient;
    @Autowired
    private IntegrationClient integrationClient;

    @GetMapping("/metrics")
    public List<MetricResponse> getAllMetrics(){
        return notationClient.getAllMetrics();
    }

    @GetMapping("/metrics/{provenance}")
    public List<MetricResponse> getAllFrom(@PathVariable String provenance){
        return notationClient.getAllFrom(provenance);
    }

    @GetMapping("/metrics/analyse/{id}")
    public List<MetricResponse> getAllFromAnalyse(@PathVariable Long id){
        return getResultatAnalyseScanner(notationClient.getAllMetrics(), id);
    }

    @GetMapping("/metrics/{id}")
    public MetricResponse getOneMetric(@PathVariable Long id){
        return notationClient.getOneMetric(id);
    }

    @PostMapping("/metrics")
    public MetricResponse saveMetric(@RequestBody MetricForm metricForm){
        return notationClient.saveMetric(metricForm);
    }

    @PutMapping("/metrics")
    public MetricResponse updateMetric(@RequestBody MetricForm metricForm){
        return notationClient.updateMetric(metricForm);
    }

    @DeleteMapping("/metrics/{id}")
    public void deleteMetric(@PathVariable Long id){
        notationClient.deleteMetric(id);
    }

    private List<MetricResponse> getResultatAnalyseScanner(List<MetricResponse> metricResponses, Long idAnlyse){
        List<ResultatScannerResponse> resultatScannerResponses = integrationClient.getResultatScannerByAnalyse(idAnlyse);
        Map<String,Double> valueIndex = resultatScannerResponses.stream()
                .collect(Collectors
                        .toMap(ResultatScannerResponse::getNameMetric,ResultatScannerResponse::getValueMetric));
        metricResponses.stream().filter( mF -> valueIndex.containsKey(mF.getNom()))
                .forEach(mF -> {
                    mF.setValeur(valueIndex.get(mF.getNom()));
                });
        return metricResponses;
    }
}
