package org.parisnanterre.korector.client.api;

import org.parisnanterre.korector.client.entities.ProjetIntegration;
import org.parisnanterre.korector.client.feigns.IntegrationClient;
import org.parisnanterre.korector.client.payload.response.AnalyseResponse;
import org.parisnanterre.korector.client.modules.teams.payloads.responses.ProjetResponse;
import org.parisnanterre.korector.client.repositories.ProjetIntegrationRepository;
import org.parisnanterre.korector.client.service.ValidationExceptionsService;
import org.parisnanterre.korector.client.modules.teams.feigns.TeamsClient;
import org.parisnanterre.korector.client.modules.teams.payloads.requests.ProjetForm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/v1")
public class ProjetController {

    private final ValidationExceptionsService validationExceptionsService;

    private final ProjetIntegrationRepository projetIntegrationRepository;

    private final TeamsClient teamsClient;

    private final IntegrationClient integrationClient;

    public ProjetController(ValidationExceptionsService validationExceptionsService, ProjetIntegrationRepository projetIntegrationRepository, TeamsClient teamsClient, IntegrationClient integrationClient) {
        this.validationExceptionsService = validationExceptionsService;
        this.projetIntegrationRepository = projetIntegrationRepository;
        this.teamsClient = teamsClient;
        this.integrationClient = integrationClient;
    }

    @GetMapping(path = "/projets/group/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_view_project')")
    ResponseEntity<String> getProjetByGroupe(@PathVariable Long id){
        return new ResponseEntity<>(teamsClient.getProjetByGroupe(id).getBody(), HttpStatus.OK);
    }

    @PostMapping(path = "/projets")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_project')")
    //todo : bug lors du add message: could not execute statement; SQL [n/a]; nested exception is org.hibernate.exception.SQLGrammarException: could not execute statement
    ResponseEntity<ProjetResponse> create(@Valid @RequestBody ProjetForm projetForm){
        ProjetResponse projetResponse = teamsClient.createProjet(projetForm).getBody();
        ProjetIntegration projetIntegration = new ProjetIntegration(projetResponse.getId());
        projetIntegrationRepository.save(projetIntegration);
        return new ResponseEntity<>(projetResponse,HttpStatus.CREATED);
    }

    @PutMapping(path = "/projets")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_project')")
    ResponseEntity<String> update(@Valid @RequestBody ProjetForm projetForm){
        return new ResponseEntity<>(teamsClient.updateProjet(projetForm).getBody(), HttpStatus.ACCEPTED);
    }

    @DeleteMapping(path = "/projets/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_project')")
    ResponseEntity<String> delete(@PathVariable Long id){
        return new ResponseEntity<>(teamsClient.deleteProjet(id).getBody(), HttpStatus.OK);
    }

    @GetMapping(path = "/projets/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_project')")
    ResponseEntity<ProjetResponse> getProjet(@PathVariable Long id){
        ProjetResponse projetResponse = teamsClient.getProjet(id).getBody();
        Set<AnalyseResponse> analyseResponseList = new HashSet<>();
        projetIntegrationRepository.getByIdProjet(id).getAnalyses()
                .forEach(analyse -> analyseResponseList.add(integrationClient.getAnalyse(analyse).getBody()));
        projetResponse.setAnalyses(analyseResponseList);
        return new ResponseEntity<>(projetResponse,HttpStatus.OK);
    }

    @GetMapping(path = "/projet/user/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_project')")
    ResponseEntity<String> getProjetsByUser(@PathVariable String userId){
        return new ResponseEntity<>(teamsClient.getProjetsByUser(userId).getBody(), HttpStatus.OK);
    }

    //Formatage des erreurs de validation
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ArrayList<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return validationExceptionsService.getErrors(ex);
    }
}
