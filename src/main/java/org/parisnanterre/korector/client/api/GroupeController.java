package org.parisnanterre.korector.client.api;

import org.parisnanterre.korector.client.service.ValidationExceptionsService;
import org.parisnanterre.korector.client.modules.teams.feigns.TeamsClient;
import org.parisnanterre.korector.client.modules.teams.payloads.requests.GroupeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1")
public class GroupeController {

    @Autowired
    private ValidationExceptionsService validationExceptionsService;

    @Autowired
    private TeamsClient teamsClient;

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_view_group')")
    @GetMapping(path = "/groupes")
    public ResponseEntity<String> getGroupes() {
        return new ResponseEntity<>(teamsClient.getGroupes().getBody(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_group')")
    @PostMapping(path = "/groupes")
    public ResponseEntity<String> createGroupe(@Valid @RequestBody GroupeForm groupe) {
        return new ResponseEntity<>(teamsClient.createGroupe(groupe).getBody(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_group')")
    @PutMapping(path = "/groupes")
    public ResponseEntity<String> updateGroupe(@Valid @RequestBody GroupeForm groupe) {
        return new ResponseEntity<>(teamsClient.updateGroupe(groupe).getBody(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_manage_group')")
    @DeleteMapping(path = "/groupes")
    public ResponseEntity<String> deleteGroupe(@RequestParam Long id) {
        return new ResponseEntity<>(teamsClient.deleteGroupe(id).getBody(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_view_group')")
    @GetMapping(path = "/groupes/{id}")
    public ResponseEntity<String> getGroupe(@PathVariable Long id){
        return new ResponseEntity<>(teamsClient.getGroupe(id).getBody(), HttpStatus.OK);
    }

    //Formatage des erreurs de validation
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ArrayList<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return validationExceptionsService.getErrors(ex);
    }
}
