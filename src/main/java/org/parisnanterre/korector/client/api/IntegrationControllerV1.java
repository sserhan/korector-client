package org.parisnanterre.korector.client.api;

import org.parisnanterre.korector.client.feigns.IntegrationClient;
import org.parisnanterre.korector.client.payload.response.AnalyseResponse;
import org.parisnanterre.korector.client.repositories.ProjetIntegrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/integration")
public class IntegrationControllerV1 {
//TODO: PreAuthorize / méthode

    private final IntegrationClient integrationClient;

    @Autowired
    private ProjetIntegrationRepository projetIntegrationRepository;

    public IntegrationControllerV1(IntegrationClient integrationClient) {
        this.integrationClient = integrationClient;
    }
    @GetMapping("/analyses/projet/{projetId}")
    public ResponseEntity<List<AnalyseResponse>> getAnalysesByProjet(@PathVariable Long projetId){
        List<AnalyseResponse> analyses = new ArrayList<>();
        projetIntegrationRepository.getByIdProjet(projetId).getAnalyses()
                .forEach(analyse -> analyses.add(integrationClient.getAnalyse(analyse).getBody()));
        return new ResponseEntity<>(analyses, HttpStatus.OK);
    }

    @GetMapping("/buildTools")
    public ResponseEntity<String> allBuildTools(){
        return new ResponseEntity<>(integrationClient.getAllBuildTools().getBody(), HttpStatus.OK);
    }

    @GetMapping("/buildTools/{id}")
    public ResponseEntity<String> getBuildTools(@PathVariable Long id){
        return new ResponseEntity<>(integrationClient.getBuild(id).getBody(), HttpStatus.OK);
    }

}
