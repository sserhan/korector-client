package org.parisnanterre.korector.client.feigns;

import org.parisnanterre.korector.client.payload.request.AnalyseForm;
import org.parisnanterre.korector.client.payload.response.AnalyseResponse;
import org.parisnanterre.korector.client.payload.response.ResultatScannerResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="integration")
public interface IntegrationClient {
    @PostMapping("/api/v1/analyses")
    AnalyseResponse buildAndSave(@RequestHeader("Authorization") String bearerToken,
                                 @RequestBody AnalyseForm analyseForm);
    @GetMapping("/api/v1/buildTools")
    ResponseEntity<String> getAllBuildTools();

    @GetMapping("/api/v1/buildTools/{id}")
    ResponseEntity<String> getBuild(@PathVariable Long id);

    @GetMapping("/api/v1/resultatScanner/{id}")
    List<ResultatScannerResponse> getResultatScannerByAnalyse(@PathVariable Long id);

    @GetMapping("/api/v1/analyses/{id}")
    ResponseEntity<AnalyseResponse> getAnalyse(@PathVariable Long id);

    @GetMapping("/api/v1/analyse/relbuild/{id}")
    AnalyseResponse rebuild(@RequestHeader("Authorization") String bearerToken, @PathVariable Long id);

}
