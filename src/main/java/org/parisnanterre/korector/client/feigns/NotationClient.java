package org.parisnanterre.korector.client.feigns;

import org.parisnanterre.korector.client.payload.request.MetricForm;
import org.parisnanterre.korector.client.payload.request.NoteForm;
import org.parisnanterre.korector.client.payload.response.MetricResponse;
import org.parisnanterre.korector.client.payload.response.NoteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "notation")
public interface NotationClient {

    //---Metrics
    @GetMapping("/api/v1/metrics")
    List<MetricResponse> getAllMetrics();
    @GetMapping("/api/v1/metrics/provenance")
    List<MetricResponse> getAllFrom(String provenance);
    @GetMapping("/api/v1/metrics/{id}")
    MetricResponse getOneMetric(@PathVariable Long id);
    @PostMapping("/api/v1/metrics")
    MetricResponse saveMetric(@RequestBody MetricForm metricForm);
    @PutMapping("/api/v1/metrics")
    MetricResponse updateMetric(@RequestBody MetricForm metricForm);
    @DeleteMapping("/api/v1/metrics/{id}")
    void deleteMetric(@PathVariable Long id);

    //---Notes
    @GetMapping("/api/v1/notes")
    List<NoteResponse> getAllNotes();
    @GetMapping("/api/v1/notes/{id}")
    NoteResponse getOneNotes(@PathVariable Long id);
    @PostMapping("/api/v1/notes/noteTotal")
    NoteResponse genererNote(@RequestBody NoteForm noteForm);
    @PostMapping("/api/v1/notes")
    NoteResponse saveNote(@RequestBody NoteForm noteForm);
    @PutMapping("/api/v1/notes")
    NoteResponse updateNote(@RequestBody NoteForm noteForm);
    @DeleteMapping("/api/v1/notes/{id}")
    void deleteNote(@PathVariable Long id);
}
