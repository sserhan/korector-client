package org.parisnanterre.korector.client.payload.request;

import java.io.Serializable;
import java.util.List;

public class NoteForm implements Serializable {
    private Long id;
    private Double noteTotal;
    private Long analyseId;
    private List<MetricForm> metrics;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getNoteTotal() {
        return noteTotal;
    }

    public void setNoteTotal(Double noteTotal) {
        this.noteTotal = noteTotal;
    }

    public Long getAnalyseId() {
        return analyseId;
    }

    public void setAnalyseId(Long analyseId) {
        this.analyseId = analyseId;
    }

    public List<MetricForm> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<MetricForm> metrics) {
        this.metrics = metrics;
    }
}
