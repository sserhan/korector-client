package org.parisnanterre.korector.client.payload.response;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClientUserResponse implements Serializable {

    private String username;

    private String avatarUrl;

    private boolean github;

    private Set<String> roles = new HashSet<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public boolean isGithub() {
        return github;
    }

    public void setGithub(boolean github) {
        this.github = github;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
