package org.parisnanterre.korector.client.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class IntegrationNote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long idAnalyse;

    @ElementCollection
    @OrderColumn(name = "notes_id")
    private Set<Long> notes = new HashSet<>();

    public IntegrationNote() {
    }

    public IntegrationNote(Long idAnalyse) {
        this.idAnalyse = idAnalyse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAnalyse() {
        return idAnalyse;
    }

    public void setIdAnalyse(Long idAnalyse) {
        this.idAnalyse = idAnalyse;
    }

    public Set<Long> getNotes() {
        return notes;
    }

    public void setNotes(Set<Long> notes) {
        this.notes = notes;
    }
}
