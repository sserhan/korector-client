package org.parisnanterre.korector.client.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ProjetIntegration {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long idProjet;

    @ElementCollection
    @OrderColumn(name = "analyses_id")
    private Set<Long> analyses = new HashSet<>();

    public ProjetIntegration() {}

    public ProjetIntegration(Long idProjet) {
        this.idProjet = idProjet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProjet() {
        return idProjet;
    }

    public void setIdProjet(Long idProjet) {
        this.idProjet = idProjet;
    }

    public Set<Long> getAnalyses() {
        return analyses;
    }

    public void setAnalyses(Set<Long> analyses) {
        this.analyses = analyses;
    }


}
