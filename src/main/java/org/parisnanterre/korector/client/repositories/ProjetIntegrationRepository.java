package org.parisnanterre.korector.client.repositories;

import org.parisnanterre.korector.client.entities.ProjetIntegration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjetIntegrationRepository extends JpaRepository<ProjetIntegration, Long> {
    ProjetIntegration getByIdProjet(Long idProjet);
}
