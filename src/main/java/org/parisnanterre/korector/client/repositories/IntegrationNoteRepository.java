package org.parisnanterre.korector.client.repositories;

import org.parisnanterre.korector.client.entities.IntegrationNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegrationNoteRepository extends JpaRepository<IntegrationNote, Long> {
    IntegrationNote getByIdAnalyse(Long idAnalyse);
}
