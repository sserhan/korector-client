package org.parisnanterre.korector.client.providers.github.payloads;

import java.io.Serializable;

public class GithubRepoResponse implements Serializable {

    private String name;

    private String provider;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
