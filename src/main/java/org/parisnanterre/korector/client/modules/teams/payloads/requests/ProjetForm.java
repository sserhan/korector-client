package org.parisnanterre.korector.client.modules.teams.payloads.requests;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class ProjetForm implements Serializable {
    private Long id;
    @NotBlank(message = "Veuillez saisir le nom du projet.")
    private String nomProjet;
    private Long groupeId;
    private String gitRepoName;
    private String gitProvider;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomProjet() {
        return nomProjet;
    }

    public void setNomProjet(String nomProjet) {
        this.nomProjet = nomProjet;
    }

    public Long getGroupeId() {
        return groupeId;
    }

    public void setGroupeId(Long groupeId) {
        this.groupeId = groupeId;
    }

    public String getGitRepoName() {
        return gitRepoName;
    }

    public void setGitRepoName(String gitRepoName) {
        this.gitRepoName = gitRepoName;
    }

    public String getGitProvider() {
        return gitProvider;
    }

    public void setGitProvider(String gitProvider) {
        this.gitProvider = gitProvider;
    }
}
