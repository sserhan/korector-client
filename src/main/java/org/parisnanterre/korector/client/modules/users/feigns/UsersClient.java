package org.parisnanterre.korector.client.modules.users.feigns;

import org.parisnanterre.korector.client.modules.users.payloads.requests.UserResponse;
import org.parisnanterre.korector.client.modules.users.payloads.responses.UserForm;
import org.parisnanterre.korector.client.modules.users.payloads.responses.UserImportResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name="users")
public interface UsersClient {

    @GetMapping("/api/v1/users")
    List<UserResponse> all(@RequestHeader("Authorization") String bearerToken);

    @GetMapping("/api/v1/users/{id}")
    UserResponse getOne(@PathVariable String id, @RequestHeader("Authorization") String bearerToken);

    @PostMapping("/api/v1/users")
    UserResponse save(@Valid @RequestBody UserForm userForm, @RequestHeader("Authorization") String bearerToken);

    @DeleteMapping("/api/v1/users/{id}")
    void delete(@PathVariable String id, @RequestHeader("Authorization") String bearerToken);

    @PutMapping("/api/v1/users")
    UserResponse update(@Valid @RequestBody UserForm userForm, @RequestHeader("Authorization") String bearerToken);

    @PostMapping("/api/v1/users/import")
    List<UserImportResponse> uploadFile(@RequestParam("file") MultipartFile file);

    @GetMapping("/api/v1/users/roles")
    List<String> getRoles(@RequestHeader("Authorization") String bearerToken);
}
