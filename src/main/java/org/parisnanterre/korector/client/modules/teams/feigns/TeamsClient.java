package org.parisnanterre.korector.client.modules.teams.feigns;

import org.parisnanterre.korector.client.modules.teams.payloads.requests.GroupeForm;
import org.parisnanterre.korector.client.modules.teams.payloads.requests.ProjetForm;
import org.parisnanterre.korector.client.modules.teams.payloads.responses.ProjetResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name="teams")
public interface TeamsClient {
    @GetMapping("/api/v1/test")
    String test();

    //---Groupes
    @GetMapping("/api/v1/groupes")
    ResponseEntity<String> getGroupes();
    @PutMapping("/api/v1/groups")
    ResponseEntity<String> updateGroupe(GroupeForm groupe);
    @PostMapping("/api/v1/groups")
    ResponseEntity<String> createGroupe(GroupeForm groupe);
    @DeleteMapping("/api/v1/groups/{id}")
    ResponseEntity<String> deleteGroupe(@PathVariable Long id);
    @GetMapping("/api/v1/groups/{id}")
    ResponseEntity<String> getGroupe(@PathVariable Long id);
    @GetMapping("/api/v1/groups/user/{userId}")
    ResponseEntity<String> getGroupesByUser(@PathVariable String userId);

    //---Projets

    @GetMapping("/api/v1/projets")
    ResponseEntity<String> getProjets();
    @PutMapping("/api/v1/projets")
    ResponseEntity<String> updateProjet(@RequestBody ProjetForm projetForm);
    @PostMapping("/api/v1/projets")
    ResponseEntity<ProjetResponse> createProjet(@RequestBody ProjetForm projetForm);
    @DeleteMapping("/api/v1/projets/{id}")
    ResponseEntity<String> deleteProjet(@PathVariable Long id);
    @GetMapping("/api/v1/projets/{id}")
    ResponseEntity<ProjetResponse> getProjet(@PathVariable Long id);
    @GetMapping("/api/v1/projets/user/{userId}")
    ResponseEntity<String> getProjetsByUser(@PathVariable String userId);
    @GetMapping("/api/v1/projets/group/{groupeId}")
    ResponseEntity<String> getProjetByGroupe(@PathVariable Long groupeId);
}
