package org.parisnanterre.korector.client.modules.teams.payloads.responses;

import org.parisnanterre.korector.client.payload.response.AnalyseResponse;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ProjetResponse implements Serializable {
    private Long id;
    private String nomProjet;
    private String nomGroupe;
    private String gitRepoName;
    private String gitProvider;
    private Set<AnalyseResponse> analyses = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomProjet() {
        return nomProjet;
    }

    public void setNomProjet(String nomProjet) {
        this.nomProjet = nomProjet;
    }

    public String getNomGroupe() {
        return nomGroupe;
    }

    public void setNomGroupe(String nomGroupe) {
        this.nomGroupe = nomGroupe;
    }

    public String getGitRepoName() {
        return gitRepoName;
    }

    public void setGitRepoName(String gitRepoName) {
        this.gitRepoName = gitRepoName;
    }

    public String getGitProvider() {
        return gitProvider;
    }

    public void setGitProvider(String gitProvider) {
        this.gitProvider = gitProvider;
    }

    public Set<AnalyseResponse> getAnalyses() {
        return analyses;
    }

    public void setAnalyses(Set<AnalyseResponse> analyses) {
        this.analyses = analyses;
    }
}
