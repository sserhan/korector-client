package org.parisnanterre.korector.client.modules.users.payloads.responses;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserForm implements Serializable {

    private String id;

    @NotBlank(message = "Veuillez saisir un nom d'utilisateur.")
    private String username;

    @Email(message = "Veuillez saisir une adresse e-mail valide.")
    @NotBlank(message = "Veuillez saisir une adresse e-mail.")
    private String email;

    @NotBlank(message = "Veuillez saisir un nom.")
    private String nom;

    @NotBlank(message = "Veuillez saisir un prénom.")
    private String prenom;

    @Size(min = 1, message = "Veuillez affecter au moins un rôle.")
    private List<String> roles = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
}
