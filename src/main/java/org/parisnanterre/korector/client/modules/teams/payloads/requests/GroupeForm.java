package org.parisnanterre.korector.client.modules.teams.payloads.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Set;

public class GroupeForm implements Serializable {

    private Long id;

    @NotBlank(message = "Veuillez saisir le nom du groupe.")
    private String nom;

    @Size(min = 1, message = "Veuillez affecter au moins un manager au groupe.")
    private Set<String> managers;

    private Set<String> users;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<String> getUsers() {
        return users;
    }

    public void setUsers(Set<String> users) {
        this.users = users;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getManagers() {
        return managers;
    }

    public void setManagers(Set<String> managers) {
        this.managers = managers;
    }
}
