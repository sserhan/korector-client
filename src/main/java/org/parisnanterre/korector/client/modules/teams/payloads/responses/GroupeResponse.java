package org.parisnanterre.korector.client.modules.teams.payloads.responses;

import java.io.Serializable;
import java.util.Set;

public class GroupeResponse implements Serializable {

    private Long id;

    private String nom;

    private Set<String> managers;

    private Set<String> users;

    private Set<ProjetResponse> projets;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<String> getUsers() {
        return users;
    }

    public void setUsers(Set<String> users) {
        this.users = users;
    }

    public Set<ProjetResponse> getProjets() {
        return projets;
    }

    public void setProjets(Set<ProjetResponse> projets) {
        this.projets = projets;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<String> getManagers() {
        return managers;
    }

    public void setManagers(Set<String> managers) {
        this.managers = managers;
    }
}
